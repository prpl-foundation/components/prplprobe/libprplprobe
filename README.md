TODO Update 

# Library Prplprobe

Prplprobe is a monitoring system designed to offer key performance indicators (KPIs) about the system (CPU, Memory, Wi-Fi, etc).

## Table of Contents

[[_TOC_]]

## Introduction

libprplprobe is a C++ library offering APIs to do:

- Ambiorix bus connection
- gRPC connection
- Generic eventing process with gRPC-based classes that implement mechanisms for collecting and sending KPIs (with or without Ambiorix)
- Configuration management
- Tracing configuration

## Installation

### Prerequisites

The follwoing packages are required to build `libprplprobe`:

- CMake

### Dependencies

`libprplprobe` relies on the following dependencies:

- Protobuf (https://github.com/protocolbuffers/protobuf)
- gRPC (https://github.com/grpc/grpc)
- libsahtrace (https://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace)
- libevent (https://github.com/libevent/libevent)

If you need to have Ambiorix support, the additionnal dependencies are required:

- libamxrt (https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxrt)

### Build from source

1. **Clone the repository**:

```sh
git clone https://gitlab.com/prpl-foundation/components/prplprobe/libprplprobe.git
cd libprplprobe
```

2. **Build the library**

```sh
mkdir build
cd build
# Without Ambiorix
cmake ..
# With Ambiorix
cmake .. -DCONFIG_SAH_LIB_PRPLPROBE_USE_AMX=y
make
make install
```

## Documentation

`libprplprobe` provides documentation for protobuf schema (protoc-gen-doc) and C++ APIs (doxygen).

### Prerequisites

The follwoing packages are required to build `libprplprobe` documentation:

- Doxygen
- Graphviz
- protoc-gen-doc (https://github.com/pseudomuto/protoc-gen-doc)

### Build doc

Both protobuf and C++ APIs documentations are generated in the same way:

```sh
cmake .. -DGEN_DOC=y
make
make install
```

## Configuration

### Build-time configuration

During the build process, you can customize `libprplprobe` by setting specific configuration options to meet your project's requirements. Below are the available build configuration options:

- **`CONFIG_SAH_LIB_PRPLPROBE_USE_AMX`**: This option allows you to enable Ambiorix features within `libprplprobe`. Set it to `y` to enable Ambiorix features.

- **`GEN_DOC`**: Enabling this option generates documentation for `libprplprobe`. Make sure to set this to `y` to generate documentation during the build process.

- **`COVERAGE`**: If you wish to include coverage flags and run tests with coverage analysis, enable this option by setting it to `y`.

To configure these options during the build process, you can set them in cmake command directly or use your buildsystem options.

## Testing

### Prerequisites

The follwoing packages are required to build `libprplprobe` documentation:
- gcovr
- valgrind

### Build and run tests

```sh
cmake .. -DCOVERAGE=y
make
```

## License

`libprplprobe` is licensed under the [BSD-2-Clause-Patent](https://spdx.org/licenses/BSD-2-Clause-Patent.html) license.

You can find a copy of the license in the [LICENSE](./LICENSE) file.

Please review the license carefully before using or contributing to this project. By participating in the `libprplprobe` community, you agree to adhere to the terms and conditions set forth in this license.

For more details about the license and its implications, refer to the [full license text](https://spdx.org/licenses/BSD-2-Clause-Patent.html).
