/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define ME "dc_lib"

#include <bus_connection.h>

#define AMXB_BACKEND_PATH  "/usr/bin/mods/amxb/"

#define AMXB_UBUS_BACKEND AMXB_BACKEND_PATH "mod-amxb-ubus.so"
#define AMXB_UBUS_URI "ubus:/var/run/ubus/ubus.sock"

#define AMXB_USP_BACKEND AMXB_BACKEND_PATH "mod-amxb-usp.so"
#define AMXB_USP_URI "usp:/var/run/usp/broker_agent_path"

static amxb_bus_ctx_t *bus_ctx = NULL;
static amxc_var_t *config = NULL;

amxb_bus_ctx_t *get_bus_ctx_priv(void) {
    return bus_ctx;
}

static void config_usp_endpoint(void) {
    char *eid = getenv("USP_ENDPOINT_ID");
    if (eid == NULL) {
        SAH_TRACEZ_ERROR(ME, "USP Endpoint ID not fount");
        return;
    }
    SAH_TRACEZ_INFO(ME, "Found USP Endpoint ID: %s", eid);

    amxc_var_new(&config);
    amxc_var_set_type(config, AMXC_VAR_ID_HTABLE);
    amxc_var_t *usp_section = amxc_var_add_key(amxc_htable_t, config, "usp", NULL);
    amxc_var_add_key(cstring_t, usp_section, "EndpointID", eid);

    if (amxb_set_config(config) != 0) {
        SAH_TRACEZ_ERROR(ME, "USP Endpoint ID configuration FAIL");
        return;
    }
    SAH_TRACEZ_INFO(ME, "USP Endpoint ID configuration Success on %s", eid);
}

static int initialize_be_usp(uint32_t access) {
    int retval = 0;

    // Load back-end
    retval = amxb_be_load(AMXB_USP_BACKEND);
    if (retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load back-end [%s], Return status = %d", AMXB_USP_BACKEND, retval);
        goto leave;
    }

    // Make sure config EndpointID is set
    config_usp_endpoint();

    // Connect to bus system
    retval = amxb_connect(&bus_ctx, AMXB_USP_URI);
    if (retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to connect to [%s], Return status = %d", AMXB_USP_URI, retval);
        amxb_be_remove("usp");
        goto leave;
    }

    // Set the access method
    amxb_set_access(bus_ctx, access);

leave:
    return retval;
}

static int initialize_be_ubus(uint32_t access) {
    int retval = 0;

    // Load back-end
    retval = amxb_be_load(AMXB_UBUS_BACKEND);
    if (retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load back-end [%s], Return status = %d", AMXB_UBUS_BACKEND, retval);
        goto leave;
    }

    // Connect to bus system
    retval = amxb_connect(&bus_ctx, AMXB_UBUS_URI);
    if (retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to connect to [%s], Return status = %d", AMXB_UBUS_URI, retval);
        amxb_be_remove("ubus");
        goto leave;
    }

    // Set the access method
    amxb_set_access(bus_ctx, access);

leave:
    return retval;
}

void connect_to_bus(uint32_t access) {
    if (initialize_be_usp(access) == 0) {
        SAH_TRACEZ_INFO(ME, "Connection successful to [usp] bus");
    }
    else if (initialize_be_ubus(access) == 0) {
        SAH_TRACEZ_INFO(ME, "Connection successful to [ubus] bus");
    }
    else {
        SAH_TRACEZ_ERROR(ME, "FAIL connecting to [usp] and [ubus] bus");
    }
}

void clean_bus_connection(void) {
    amxb_free(&bus_ctx);
    bus_ctx = NULL;
    amxc_var_delete(&config);
    amxb_be_remove_all();
}