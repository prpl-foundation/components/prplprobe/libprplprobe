# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.2.0 - 2025-01-16(10:18:38 +0000)

### Other

- - [Prpl] [New arch] Add wifi collector

## Release v1.0.0 - 2024-08-09(13:32:04 +0000)

### Other

- - [Prpl][Lib] - New lib functions

## Release v0.0.9 - 2024-04-05(12:26:38 +0000)

### Other

- - [Prpl] Fix amxrt connection to the bus

## Release v0.0.8 - 2024-01-23(17:10:16 +0000)

### Other

- - [Prpl] WiFi Scan event must have exactly the same format than on SOP

## Release v0.0.7 - 2023-11-02(07:22:24 +0000)

### Other

- - [Eyes'ON][prpl] Document Eyes'ON Prpl APIs

## Release v0.0.6 - 2023-10-17(07:28:54 +0000)

### Other

- - [Prpl] Compile containers with ??? version of LCM SDK for Network X

## Release v0.0.5 - 2023-10-09(14:51:41 +0000)

### Other

- - [Prpl] Open source delivery process

## Release v0.0.4 - 2023-10-06(11:04:23 +0000)

### Other

- Opensource Component

## Release v0.0.3 - 2023-09-29(19:28:36 +0000)

### Other

- - [Eyes'ON][prpl] Add a tool to decode base64 encoded protobuf message received on MQTT broker

## Release v0.0.2 - 2023-09-19(08:13:52 +0000)

### Other

- - [Eyes'ON][prpl] Add CI tests on new Eyes'ON prpl components
- - [Prpl] Rename all "agent" into "prplprobe agent"

## Release v0.0.1 - 2023-08-25(12:46:47 +0000)

### Other

- - [Eyes'ON][prpl] Add a client lib to have common code and simplify integration of new modules
- - [Eyes'ON][prpl] Make SAH CI work on agent and modules
- - [Prpl] Package for open source delivery

